
# Web Log Parser

## Description
Parses a web server log file, returns the 10 most frequently requested objects and their cumulative 
bytes transferred. Only includes GET requests with Successful (HTTP 2xx) responses. The log format 
is:
- request date, time, and time zone
- request line from the client
- HTTP status code returned to the client - size (in bytes) of the returned object
Given this input data:
```
[01/Aug/1995:00:54:59 -0400] "GET /images/opf-logo.gif HTTP/1.0" 200 32511
[01/Aug/1995:00:55:04 -0400] "GET /images/ksclogosmall.gif HTTP/1.0" 200 3635
[01/Aug/1995:00:55:06 -0400] "GET /images/ksclogosmall.gif HTTP/1.0" 403 298
[01/Aug/1995:00:55:09 -0400] "GET /images/ksclogosmall.gif HTTP/1.0" 200 3635
[01/Aug/1995:00:55:18 -0400] "GET /images/opf-logo.gif HTTP/1.0" 200 32511
[01/Aug/1995:00:56:52 -0400] "GET /images/ksclogosmall.gif HTTP/1.0" 200 3635
[01/Aug/1995:00:56:52 -0400] "POST /images/rickjames.gif HTTP/1.0" 201 7000
[01/Aug/1995:00:56:52 -0400] "GET /images/rickjames.gif HTTP/1.0" 202 7000
[01/Aug/1995:00:56:52 -0400] "GET /images/rickjames.gif HTTP/1.0" 206 7000
[01/Aug/1995:00:56:52 -0400] "GET /images/rickjames.gif HTTP/1.0" 300 7000
```
The result should be:
```
/images/ksclogosmall.gif 10905
/images/opf-logo.gif 65022
/images/rickjames.gif 14000
```
or
```
/images/ksclogosmall.gif 10905
/images/rickjames.gif 14000
/images/opf-logo.gif 65022
```
since there is no deterministic order when there is a tie.

## Running Solution

```bash
$ python3 runme.py -h
usage: runme.py [-h] [--n N] [--realtime REALTIME] log

find top N most successfully requested objects

positional arguments:
  log                  full path to the web log file

optional arguments:
  -h, --help           show this help message and exit
  --n N                number of objects to find (default 10)
  --realtime REALTIME  see top results in realtime as log is parsed (default
                       False)
```

Example,
```bash
python3 runme.py <web_log_file> --realtime 1
```
