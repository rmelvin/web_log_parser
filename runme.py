import argparse
from collections import Counter

from requests.status_codes import codes

rows = []


def _is_truthy(value) -> bool:
    """Detect if the value is truthy."""
    return value in (True, 'True', 'true', 1, '1')


def read_file(file) -> list:
    """Parse web log file and return important bits (object, response, num_bytes, method)."""
    with open(file) as fptr:
        requests = fptr.readlines()
        for request in requests:
            request = request.replace('"', '')
            datetime_, timezone_, method, object_, proto, response, num_bytes = request.split()
            # coerce response to int now
            try:
                response = int(response)
            except ValueError:
                continue
            # coerce num bytes to int now
            try:
                num_bytes = int(num_bytes)
            except ValueError:
                continue
            yield object_, response, num_bytes, method


def only_get_requests(data):
    """Filter row data by if the request method is GET."""
    for row in data:
        if 'GET' == row[3]:
            yield row


def success_objects(data):
    """Filter row data by if it has a successful response (i.e. status is 2xx)."""
    for row in data:
        if codes.ok <= row[1] < codes.multiple_choices:  # ok=200, multiple_choices=300
            yield row


def find_topn(row, n: int=10):
    """
    Find top N most requested objects at the current moment in time.
    Also, compute sum only for the most requested objects.
    """
    global rows
    rows.append(row)
    ctr = Counter((row[0] for row in rows))

    # Only compute sum for most common objects.
    topn = []
    for object_, _ in ctr.most_common(n):
        total_bytes = sum((row_[2] for row_ in rows if object_ == row_[0]))
        topn.append((object_, total_bytes))
    return topn


if __name__ == '__main__':
    # Parse command-line args
    parser = argparse.ArgumentParser(description='find top N most successfully requested objects')
    parser.add_argument('log', type=str, help='full path to the web log file')
    parser.add_argument('--n', type=int, default=10, help='number of objects to find (default 10)')
    parser.add_argument('--realtime', type=str, default=False,
                        help='see top results in realtime as log is parsed (default False)')
    args = parser.parse_args()

    # Read web log file.
    results = []
    real_time = _is_truthy(args.realtime)
    for row in success_objects(only_get_requests(read_file(args.log))):
        if real_time:
            # Display top N results as log is parsed.
            for object_, total_bytes in find_topn(row, n=args.n):
                print(object_, total_bytes)
            print()
        else:
            results.append(find_topn(row, n=args.n))
    if not real_time:
        # Get top N results for last row.
        for object_, total_bytes in results[-1]:
            print(object_, total_bytes)
